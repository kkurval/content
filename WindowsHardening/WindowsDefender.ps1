#Run PowerShell as administrator

#Command-Line option (PowerShell):

$WindowsUpdateService=Get-Service -DisplayName *Win*Up* 
$WindowsUpdateService
$WindowsUpdateService | Set-Service -StartupType Automatic
$WindowsUpdateService
$WindowsUpdateService | Start-Service
$WindowsUpdateService



Get-Command -Module Defender

Update-MpSignature

Get-MpComputerStatus
Get-MpComputerStatus | Select-Object *Enabled*
Get-MpComputerStatus | Select-Object *SignatureAge*

Update-MpSignature
MpCmdRun.exe -SignatureUpdate 


Get-MpPreference
Get-MpPreference | Select-Object *Disable* | findstr True 

Set-MpPreference -DisableCatchupFullScan False
Set-MpPreference -DisableCatchupQuickScan
Set-MpPreference -DisableEmailScanning
Set-MpPreference -DisableIntrusionPreventionSystem
Set-MpPreference -DisableRemoveableDriveScanning
Set-MpPreference -DisableScanningMappedNetworkDrivesForFullScan
