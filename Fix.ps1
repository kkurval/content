$Template_Klist_Sessions=@'
[{ID*:0}] Session {IDS:0} {IDL:0:0xb5786c7} {AC:NT VIRTUAL MACHINE\B3D2EA99-3195-4DCA-9EF7-5BB640D95A2B} {AUTH:Negotiate:Service}
[{ID*:1}] Session {IDS:14} {IDL:0:0xb5735d0} {AC:DESKTOP-BQFSQGG\KK} {AUTH:NTLM:Interactive}
[{ID*:2}] Session {IDS:14} {IDL:0:0xb573583} {AC:DESKTOP-BQFSQGG\KK} {AUTH:NTLM:Interactive}
[{ID*:3}] Session {IDS:14} {IDL:0:0xb49e63a} {AC:Window Manager\DWM-14} {AUTH:Negotiate:Interactive}
[{ID*:36}] Session {IDS:0} {IDL:0:0x3e4} {AC:WORKGROUP\DESKTOP-BQFSQGG$} {AUTH:Negotiate:Interactive}
[{ID*:39}] Session {IDS:0} {IDL:0:0x3e7} {AC:WORKGROUP\DESKTOP-BQFSQGG$} {AUTH:NTLM:(0)}
'@

$Template_Klist_Id='{Current_ID:Current LogonId is 0:0x*}'

$Template_Klist_Tickets=@'

Current LogonId is 0:0xb4ffc69

Cached Tickets: (4)

#{[int]ID*:0}>	Client: {Client:Administrator @ CONTOSO.COM}
	Server: {Server:krbtgt/CONTOSO.COM @ CONTOSO.COM}
	KerbTicket Encryption Type: {KerbTicketEncryptionType:AES-256-CTS-HMAC-SHA1-96}
	Ticket Flags {TicketFlags:0x60a10000} -> {TicketFlagsEnum:forwardable forwarded renewable pre_authent name_canonicalize} 
	Start Time: {[datetime]StartTime:8/29/2016 16:06:22} (local)
	End Time:   {[datetime]EndTime:8/30/2016 2:06:21} (local)
	Renew Time: {[datetime]RenewTime:9/5/2016 16:06:21} (local)
	Session Key Type: {SessionKeyType:AES-256-CTS-HMAC-SHA1-96}
	Cache Flags: {CacheFlags:0x1} -> {CacheFlagsEnum:PRIMARY} 
	Kdc Called: {KdcCalled:2012R2-DC.contoso.com}

#{[int]ID*:1}>	Client: {Client:Administrator @ CORP.CONTOSO.COM}
	Server: {Server:krbtgt/CONTOSO.COM @ CONTOSO.COM}
	KerbTicket Encryption Type: {KerbTicketEncryptionType:AES-256-CTS-HMAC-SHA1-96}
	Ticket Flags {TicketFlags:0x40e10000} -> {TicketFlagsEnum:forwardable renewable initial pre_authent name_canonicalize} 
	Start Time: {[datetime]StartTime:8/29/2016 16:06:21} (local)
	End Time:   {[datetime]EndTime:8/30/2016 2:06:21} (local)
	Renew Time: {[datetime]RenewTime:9/5/2016 16:06:21} (local)
	Session Key Type: {SessionKeyType:AES-256-CTS-HMAC-SHA1-96}
	Cache Flags: {CacheFlags:0x2} -> {CacheFlagsEnum:DELEGATION} 
	Kdc Called: {KdcCalled:2012R2-DC.contoso.com}

#{[int]ID*:2}>	Client: {Client:Administrator @ CORP.NA.ALPINESKIHOUSE.COM}
	Server: {Server:host/2012R2-MS.contoso.com @ CONTOSO.COM}
	KerbTicket Encryption Type: {KerbTicketEncryptionType:AES-256-CTS-HMAC-SHA1-96}
	Ticket Flags {TicketFlags:0x40a10000} -> {TicketFlagsEnum:forwardable renewable pre_authent name_canonicalize} 
	Start Time: {[datetime]StartTime:8/29/2016 1:12:25} (local)
	End Time:   {[datetime]EndTime:8/30/2016 2:06:21} (local)
	Renew Time: {[datetime]RenewTime:9/5/2016 1:06:21} (local)
	Session Key Type: {SessionKeyType:AES-256-CTS-HMAC-SHA1-96}
	Cache Flags: {CacheFlags:0} 
	Kdc Called: {KdcCalled:2012R2-DC}

#{[int]ID*:3}>	Client: {Client:Administrator @ CONTOSO.COM}
	Server: {Server:RPCSS/2012R2-MS.contoso.com @ CONTOSO.COM}
	KerbTicket Encryption Type: {KerbTicketEncryptionType:RSADSI RC4-HMAC(NT)}
	Ticket Flags {TicketFlags:0x40a10000} -> {TicketFlagsEnum:forwardable renewable pre_authent name_canonicalize} 
	Start Time: {[datetime]StartTime:12/29/2016 16:12:25} (local)
	End Time:   {[datetime]EndTime:12/30/2016 12:06:21} (local)
	Renew Time: {[datetime]RenewTime:12/5/2016 16:06:21} (local)
	Session Key Type: {SessionKeyType:RSADSI RC4-HMAC(NT)}
	Cache Flags: {CacheFlags:0} 
	Kdc Called: {KdcCalled:2012R2-DC.contoso.com}
'@

Function Get-Session-Info {
	hostname
	date
	$regexa = '.+Domain="(.+)",Name="(.+)"$'
	$regexd = '.+LogonId="(\d+)"$'
	$logon_users = @(Get-WmiObject win32_loggedonuser -ComputerName 'localhost')

	$session_user = @{}
	$logon_users |% {
		$_.antecedent -match $regexa > $nul
		$username = $matches[1] + "\" + $matches[2]
		$_.dependent -match $regexd > $nul
		$session = $matches[1]
		$sessionHex = ('0x{0:X}' -f [int]$session)
		$session_user[$sessionHex] += $username 
				  
	}
	$session_user | ft -wrap
	$session_user | ft -wrap > Session.log
	
}

Function Get-Session-Info2 {
	hostname
	date
	$A=Get-WmiObject Win32_SessionProcess
	#$A.Dependent -split "=")[-1] -replace '"'  -replace "}","").Trim()
	$A.Dependent
	$A.Dependent > Session2.log
	
}


Function Get-Session-Info3 {
	hostname
	date
	$Template_Klist_Sessions > template_klist_sessions.txt
	$a = klist sessions | ConvertFrom-String -TemplateFile .\template_klist_sessions.txt |ft -wrap #| Out-GridView
	$a > Session3.log
}

Function Get-Session-Processes {
	# iwr https://live.sysinternals.com/logonsessions64.exe -usebasicparsing
	.\logonsessions64.exe
	$a=(.\logonsessions64.exe -c -nobanner -p)
	$a[0]="L"+$a[0]
	$a > Temp3.csv
	$h4=Import-Csv -Path Temp3.csv
}

Function Get-Process-Info {
	hostname
	date
	#$Powershell_Processes = (Get-WmiObject Win32_Process -Filter "name = 'powershell.exe'").Commandline
	$Powershell_Processes = (Get-WmiObject Win32_Process).Commandline
	$Powershell_Processes | findstr powershell
	$Powershell_Processes | findstr bypass
	$Powershell_Processes > Process.log
	
	$process = Get-Process
<#
	Get-WmiObject Win32_SessionProcess | ForEach-Object {

		$userid = (($_.Antecedent -split “=”)[-1] -replace '"'  -replace “}”,“”).Trim()
		if($users.ContainsKey($userid))
		{
			#Get username from cache
			$username = $users[$userid]
		} 
		else 
		{
			$username = (Get-WmiObject -Query "ASSOCIATORS OF {Win32_LogonSession.LogonId='$userid'} WHERE ResultClass=Win32_UserAccount").Name
			#Cache username
			$users[$userid] = $username
		}

		$procid = (($_.Dependent -split “=”)[-1] -replace '"'  -replace “}”,“”).Trim()
		$proc =  $process | Where-Object { $_.Id -eq $procid }

		New-Object psobject -Property @{
			UserName = $username
			ProcessName = $proc.Name
			"WorkingSet(MB)" = $proc.WorkingSet / 1MB
		}
	}
#>
}

Function Get-Process-Network-Info {
	Get-NetTCPConnection -OwningProcess 2948
	# netstat -aon|findstr 2948
}

# AD USERS 

Function Get-AD-Users {
	hostname
	date
	(Get-ADUser -Filter * -Properties * | Sort-Object Modified -Descending).Name
	$AD_Admins = Get-ADGroupMember -Identity "Domain Admins" -Recursive | %{Get-ADUser -Identity $_.distinguishedName -Properties *} 
	$New_AD_Admins = $AD_Admins | Select-Object Name, Created, Modified
	$New_AD_Admins > User.log
}

# SAMBA

Function Monitor-SMB-Sessions {
	hostname
	date
	$Log='Sessions.log'
	[int]$i=0
	Date
	net sessions > $Log ; while ($i -le 150) { net session | findstr /v "successfully. -------- Idle">> $Log ; Start-Sleep 1 ; $i++}
	$SMB_Sessions = type $Log | findstr \ | findstr /V 192.168.0.200 | findstr /V ::1 | findstr /V 127.0.0.1
	$Log > SMBSessionsFull.log
	$SMB_Sessions > SMBSessions.log
	$SMB_Sessions
}

Function Get-SMB-Info {
	hostname
	date
	$a = {
	get-smbserverconfiguration
	Get-SmbServerConfiguration | Select EnableSMB1Protocol, EnableSMB2Protocol
	get-smbopenfile
	get-smbsession -includehidden
	get-smbconnection
	net file
	net session
	} | IEX
	$a > SMBInfo.log
	$a
}

# LOGGING 

Function Get-Log-Security {
	hostname
	date
	$FilterHashTable = @{
		LogName      = 'Security'
		#ProviderName = 'Microsoft-Windows-Security-Auditing' 
		#Path = <String[]>
		#Keywords = <Long[]>
		ID           = 4624
		#Level = <Int32[]>
		StartTime    = (Get-Date).AddDays(-1)
		EndTime      = Get-Date
		#UserID = <SID>
		#Data = <String[]>
	}
	$Event1 = Get-WinEvent -FilterHashtable $FilterHashTable
	$Event2 = $Event1 | ForEach-Object {write-output (($_.Properties[11].Value) + " : " + ($_.Properties[18].Value) +" : " + ($_.Properties[19].Value))}
	$Event2 | findstr /V ::1 | findstr /V -
	echo 'If no output look into Event1'
	$Event2 > Security.log
	# A lot faster. Lacking process info and event correlation 
	
	#Get-EventLog -LogName Security | Where-Object {$_.EventID -eq 63} |  Select-Object -Property Source, EventID, InstanceId, Message

	#$Events = Get-EventLog -LogName System -Newest 1000
	#$Events | Group-Object -Property Source -NoElement | Sort-Object -Property Count -Descending
}


##########################################################################################################################################
##########################################################################################################################################

Function Set-WallPaper($Value) {
 Set-ItemProperty -path 'HKCU:\Control Panel\Desktop\' -name wallpaper -value $value
 rundll32.exe user32.dll, UpdatePerUserSystemParameters
}



Function Enumeration {
	# Empire 
	Get-ComputerDetails.ps1
	Invoke-WinEnum.ps1
	Get-SystemDNSServer.ps1
	# Nishang
	Get-Information.ps1
}

Function Update-Win8 {
	$WindowsUpdateService=Get-Service -DisplayName *Win*Up* 
	$WindowsUpdateService
	$WindowsUpdateService | Set-Service -StartupType Automatic
	$WindowsUpdateService
	$WindowsUpdateService | Start-Service
	$WindowsUpdateService
	
	$UpdateSession = New-Object -ComObject Microsoft.Update.Session
	$UpdateSearcher = $UpdateSession.CreateupdateSearcher()
	$Updates = @($UpdateSearcher.Search("IsHidden=0 and IsInstalled=0").Updates)
	$Updates | Select-Object Title
	#https://gallery.technet.microsoft.com/Get-WindowsUpdatesps1-7c82c1f4/file/70285/1/Get-WindowsUpdates.ps1

	#https://github.com/proxb/PoshPAIG
	#Get-WUInstall -MicrosoftUpdate -Category "Security Updates" -AcceptAll -AutoReboot -Verbose
	Get-Service -Displayname *Win*Up* | fl
	Get-WmiObject -Class Win32_Service -Property StartMode -Filter "DisplayName='Windows Update'"
	Get-Service -Displayname *Win*Up* | Set-Service -StartupType Automatic
	Start-Service -Displayname "Windows Update"
	[version](New-Object -ComObject Microsoft.Update.AgentInfo)
	[version]((Get-Item C:\Windows\system32\wuaueng.dll).VersionInfo.FileVersion -split "\s")[0]
	
	Install-Module -Name PSWindowsUpdate
	Get-Command -Module PSWindowsUpdate

	Get-WUList -Category 'Security Updates','Critical Updates'
	#https://docs.microsoft.com/en-us/powershell/wmf/setup/install-configure
	#http://download.microsoft.com/download/6/F/5/6F5FF66C-6775-42B0-86C4-47D41F2DA187/Win8.1AndW2K12R2-KB3191564-x64.msu

}



Function Fix-Patch-Win8 {
	$KB4056898='http://download.windowsupdate.com/c/msdownload/update/software/secu/2018/01/windows8.1-kb4056898-v2-x64_754f420c1d505f4666437d06ac97175109631bf2.msu'
	$KB3139914='http://download.windowsupdate.com/d/msdownload/update/software/secu/2016/02/windows8.1-kb3139914-x64_c0ed0734cde5d1eddcc7ad0632eb539e766a9e93.msu'

	(New-Object System.Net.WebClient).DownloadFile($KB4056898, "C:\Emuldata\windows8.1-kb4056898-v2-x64_754f420c1d505f4666437d06ac97175109631bf2.msu")
	(New-Object System.Net.WebClient).DownloadFile($KB3139914, "C:\Emuldata\windows8.1-kb3139914-x64_c0ed0734cde5d1eddcc7ad0632eb539e766a9e93.msu")

	# Attack 4 # Install patches for 2 exploits 
	wusa.exe 'C:\Emuldata\windows8.1-kb3139914-x64_c0ed0734cde5d1eddcc7ad0632eb539e766a9e93.msu /quiet /norestart'
	wusa.exe 'C:\Emuldata\windows8.1-kb4056898-v2-x64_754f420c1d505f4666437d06ac97175109631bf2.msu /quiet'
	shutdown -r -t 1
}

Function Fix-Attack-1 { 
	## Attack 1 # Enable Windows update service for Defender

	Get-Command -Module *Defender*
	Update-MpSignature
	Get-MpComputerStatus

	Get-MpPreference | Select-Object *Disable* | findstr True 
	Get-MpComputerStatus | Select-Object *Enabled*
	Get-MpComputerStatus | Select-Object *SignatureAge*
	
	Update-MpSignature
	echo '"C:\Program Files\Windows Defender\MpCmdRun.exe" -SignatureUpdate' | cmd
	Get-MpComputerStatus

	$WindowsUpdateService=Get-Service -DisplayName *Win*Up* 
	$WindowsUpdateService
	$WindowsUpdateService | Set-Service -StartupType Automatic
	$WindowsUpdateService
	$WindowsUpdateService | Start-Service
	$WindowsUpdateService

	Update-MpSignature
	echo '"C:\Program Files\Windows Defender\MpCmdRun.exe" -SignatureUpdate' | cmd
	Get-MpComputerStatus
	
	Get-MpPreference
	Set-MpPreference -DisableCatchupFullScan $False
	Set-MpPreference -DisableCatchupQuickScan $False
	Set-MpPreference -DisableEmailScanning $False
	Set-MpPreference -DisableIntrusionPreventionSystem $False
	Set-MpPreference -DisableRemoveableDriveScanning $False
	Set-MpPreference -DisableScanningMappedNetworkDrivesForFullScan $False
	
	Get-MpThreatDetection
	Get-MpThreatDetection | Select-Object DomainUser, InitialDetectionTime, LastThreatStatusChangeTime, ProcessName, Resources, ThreatID

	echo '"C:\Program Files\Windows Defender\MpCmdRun.exe" -SignatureUpdate' | cmd
}

Function Fix-Attack-2 {
	# Attack 2 # Disable SMB1 to break the 2x pass the hash (Already gets fixed by firewall settings)
	# Disable local admin
	# Disable LAPS
	# Enable Firewall
	# Disable Network NTML
	Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart
}

Function Fix-Attack-3 {
	# Attack 3 # Enable firewall. Block PowerShell from connecting out
	# Will also fix Attack 4 due to the filewall changes
	netsh advfirewall firewall add rule name="PS-Deny-All" dir=out action=block program="c:\windows\system32\WindowsPowerShell\v1.0\powershell.exe" enable=yes profile=domain
	Set-NetFirewallProfile -Profile Domain -Enabled True
	#Restart-Computer
}

Function Fix-Attack-3-Alt {
	# Block powershell.exe rights
	TAKEOWN /F C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe
	$Acl = Get-Acl "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	$Ar = New-Object System.Security.AccessControl.FileSystemAccessRule("Users", "Read", "None", "None", "Allow")
	$Acl.SetAccessRule($Ar)
	Set-Acl "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" $Acl
	#Restart-Computer
}

Function Fix-Attack-4 {
	Fix-Patch-Win8
	#Update-Win8
}

Function Fix-Attack-7 {
	# Attack 7 # Reset krbtgt hash to deactivate golden ticket. 
	net user krbtgt Sa1aKa1a123
	net user krbtgt Sa1aKa1a123
}

Function Fix-Attack-5 {
	# Attack 5 # Disable SMB1 to break the pass the hash file upload.
	Disable-WindowsOptionalFeature -Online -FeatureName smb1protocol -NoRestart
}

Function Fix-Attack-6 {
	#Fix-Attack-5
	#Fix-Attack-7
	# Attack 6 # Remove malicious user
	Remove-ADUser -Identity 'Elva Ray' -Confirm:$false
}

Function Configure-WinRM {
	$Win8_IP='192.168.0.2'
	$Win2016_IP='192.168.0.200' 
	Start-Service -Name WinRM
	Enable-PSRemoting -SkipNetworkProfileCheck -Force
	Set-Item WSMan:\localhost\Client\TrustedHosts -Value $Win2016_IP -Force
	Set-Item WSMan:\localhost\Client\TrustedHosts -Value $Win8_IP  -Force
	# FAILS ON FIRST TRY FROM FUNCTION
	WMIC /NODE:$Win8_IP PROCESS CALL CREATE "powershell Get-Service -Name WinRM | Start-Service"
	WMIC /NODE:$Win8_IP  PROCESS CALL CREATE "powershell Enable-PSRemoting -SkipNetworkProfileCheck -Force"
}

Function Fix-All {
	# Run on Windows 10 host as BOB. Using ADMINISTRATOR rights.
	
	# SEEMS TO FAIL AT START ... ?
	Date
	echo 'Sleeping until host has ran 500 seconds to let other setup finish'
	$wait=(get-ciminstance -ClassName Win32_OperatingSystem | Select lastbootuptime).lastbootuptime
	While (((Date)- $wait).TotalSeconds -le 500) {Start-Sleep 1}
	Date
	
	Configure-WinRM
	#Connect-PSSessions
	
	$Win8_IP='studentPC'
	$Win2016_IP='North'
	
	$Username='northwindtraders.com\Bob'
	$Password='Sa1aKa1a'

	$Target_User=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username, (ConvertTo-SecureString -String $Password -AsPlainText -Force)
	
	$Win2016=New-PSSession -ComputerName $Win2016_IP -Credential $Target_User -Authentication Negotiate
	$Win8=New-PSSession -ComputerName $Win8_IP -Credential $Target_User -Authentication Negotiate
    
    while($true) {
        if(!($Win2016)) {
            $Win2016=New-PSSession -ComputerName $Win2016_IP -Credential $Target_User -Authentication Negotiate
        } else {echo 'PSSession 2016 up'}
        if(!($Win8)) {
            $Win8=New-PSSession -ComputerName $Win8_IP -Credential $Target_User -Authentication Negotiate
        } else {echo 'PSSession 8 up'}
        if($Win8 -and $Win2016) {
            Break
        }
		Start-Sleep 1
    }
	
	# Windows 8.1 StudentPC 192.168.0.2
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Fix-Attack-1}
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Fix-Attack-2}
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Fix-Attack-3}
	# Can fail download
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Fix-Patch-Win8} # Fix-Attack-4 Fix-Patch-Win8 # Cannot pass nested functions
	#Invoke-Command -Session $Win8 -ScriptBlock {shutdown -r -t 1}
	wmic /node:192.168.0.2 qfe list |findstr 3139914 |findstr 4056898
	
	# Windows 2016 server North 192.168.0.200
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Fix-Attack-5}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Fix-Attack-7}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Fix-Attack-6} # Cannot pass nested functions, attack 5, 7
	Invoke-Command -Session $Win2016 -ScriptBlock {shutdown -r -t 1}
}

Function Detection {
	Date
	echo 'Sleeping until host has ran 500 seconds to let other setup finish'
	$wait=(get-ciminstance -ClassName Win32_OperatingSystem | Select lastbootuptime).lastbootuptime
	While (((Date)- $wait).TotalSeconds -le 500) {Start-Sleep 1}
	Date
	
	Configure-WinRM
	
	$Win8_IP='studentPC'
	$Win2016_IP='North'
	
	$Username='northwindtraders.com\Bob'
	$Password='Sa1aKa1a'

	$Target_User=New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Username, (ConvertTo-SecureString -String $Password -AsPlainText -Force)
	
	$Win2016=New-PSSession -ComputerName $Win2016_IP -Credential $Target_User -Authentication Negotiate
	$Win8=New-PSSession -ComputerName $Win8_IP -Credential $Target_User -Authentication Negotiate
    
    while($true) {
        if(!($Win2016)) {
            $Win2016=New-PSSession -ComputerName $Win2016_IP -Credential $Target_User -Authentication Negotiate
        } else {echo 'PSSession 2016 up'}
        if(!($Win8)) {
            $Win8=New-PSSession -ComputerName $Win8_IP -Credential $Target_User -Authentication Negotiate
        } else {echo 'PSSession 8 up'}
        if($Win8 -and $Win2016) {
            Break
        }
		Start-Sleep 1
    }
	
		# Windows 8.1 StudentPC 192.168.0.2
		
	#    INSTALL WINDOWS 8 powershell core 6 for TCPSessions and smbconnections (also the regex parser)	
	
	echo 'Get logon info'
	Start-Sleep 1 # EVENT LOG PARSING
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Get-Log-Security}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Get-Log-Security}
	
	echo 'Get session info'
	Start-Sleep 1
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Get-Session-Info}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Get-Session-Info}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Get-Session-Info3} # template. 
	
	echo 'Get SMB info'
	Start-Sleep 1
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Get-SMB-Info}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Get-SMB-Info}
	# run in the background
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Monitor-SMB-Sessions}
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Monitor-SMB-Sessions}
	
	echo 'Get domain admin info'
	Start-Sleep 1
	Invoke-Command -Session $Win2016 -ScriptBlock ${Function:Get-AD-Users}
	
	echo 'Get process info'
	Start-Sleep 1
	Invoke-Command -Session $Win8 -ScriptBlock ${Function:Get-Process-Info}	
}
